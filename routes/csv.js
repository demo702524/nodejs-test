'use strict';
var express = require('express');
const tools = require('../function/tools');
var router = express.Router();
const multer = require('multer');
const csv = require('csv-parser');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, tools.formatDateToYYMMDDHHSS() + "_" + file.originalname);
  }
});
const upload = multer({ storage: storage });

router.post('/', upload.single('file'), async (req, res) => {
  const get_csv = async (filePath) => {
    return new Promise((resolve, reject) => {
      const results = [];
      fs.createReadStream(filePath)
        .pipe(csv({ separator: '\\' }))
        .on('data', (data) => {
          let transformedData = {};
          for (let key in data) {
            transformedData[key] = data[key];
          }
          results.push(transformedData);
        })
        .on('end', () => {
          resolve(results);
        })
        .on('error', (error) => {
          reject(error);
        });
    });
  };
  const file = req.file;
  if (!file) {
    res.status(400).send('No file uploaded.');
    return;
  }
  const filePath = file.path;
  const result = await get_csv(file.path);
  res.status(200).send(result);
});
router.post('/q', async (req, res) => {
  try {
    const result = await tools.Query();
    res.send(result);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});
module.exports = router;