'use strict';
var express = require('express');
var router = express.Router();

router.post('/aa', (req, res) => {
    let response = { "test": "" };
    let body = req.body;
    const sql = require('mssql');
    const config = {
        user: 'sa',
        password: 'sa',
        server: '192.168.50.82',
        database: 'BPMPro',
        options: {
            encrypt: true,
            trustServerCertificate: true,
        },
    };
    async function runQuery () {
        try {
            await sql.connect(config);
            const result = await sql.query('SELECT * FROM BA_Data');
            console.log(result.recordset);
            await sql.close();
            return result.recordset;
        } catch (err) {
            console.error(err);
        }
    }
    runQuery().then(result => {
        response["runQuery"] = result;
        res.send(response);
    }).catch(err => {
        console.error(err);
        res.status(500).send('錯誤');
    });
});

module.exports = router;
