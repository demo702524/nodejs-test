'use strict';
var express = require('express');
var router = express.Router();
const http = require('http');
const os = require('os');
/* GET home page. */
router.get('/', function (req, res) {
  let obj = {
    "本機IP地址": getLocalIPAddresses(),
    "本機主機名": os.hostname(),
    "Node.js 版本": process.version,
    "運行環境平台": process.platform,
    "處理器架構": process.arch
  }
  res.send(obj);
});
function getLocalIPAddresses () {
  const interfaces = os.networkInterfaces();
  const addresses = [];
  for (const interfaceName in interfaces) {
    const networkInterface = interfaces[interfaceName];
    for (const interfaceInfo of networkInterface) {
      if (interfaceInfo.family === 'IPv4' && !interfaceInfo.internal) {
        addresses.push(interfaceInfo.address);
      }
    }
  }
  return addresses;
}
module.exports = router;
