'use strict';
const fs = require('fs');
const { promisify } = require('util');
const readFileAsync = promisify(fs.readFile);
const sql = require('mssql');

module.exports.formatDateToYYMMDDHHSS = () => {
  const now = new Date();
  const year = now.getFullYear().toString().slice(-2);
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const day = String(now.getDate()).padStart(2, '0');
  const hours = String(now.getHours()).padStart(2, '0');
  const minutes = String(now.getMinutes()).padStart(2, '0');
  const seconds = String(now.getSeconds()).padStart(2, '0');
  return year + month + day + hours + minutes + seconds;
};

module.exports.ReadSetting = () => {
  fs.readFile('Web.config', 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
    parseString(data, (err, result) => {
      if (err) {
        console.error(err);
        return;
      }
      const appSettings = result.configuration.appSettings[0].add;
      const appSettingsObj = {};
      appSettings.forEach(setting => {
        const key = setting.$.key;
        const value = setting.$.value;
        appSettingsObj[key] = value;
      });
      console.log(appSettingsObj);
    });
  });
}

const config = async (key) => {
  try {
    const data = await readFileAsync('config/config.json', 'utf8');
    const jsonData = JSON.parse(data);
    return jsonData[key];
  } catch (err) {
    console.error('config-err：', err);
    throw err;
  }
};

module.exports.Query = async () => {
  const runQuery = async (DbConnect) => {
    try {
      await sql.connect(DbConnect);
      const result = await sql.query('SELECT * FROM BA_Data');
      sql.close();
      return result.recordset;
    } catch (err) {
      console.error('runQuery-err：', err);
    }
  }
  try {
    const DbConnect = await config("DbConnect"); // 等待获取正确的 DbConnect 值
    const response = {};
    const result = await runQuery(DbConnect);
    response["runQuery"] = result;
    return response;
  } catch (err) {
    console.error('Query-err：', err);
    return null;
  }
}
